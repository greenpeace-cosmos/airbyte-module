resource "google_service_account" "bigquery_account" {
  account_id   = "${var.nro}-bigquery-sa"
  display_name = "${var.nro}-SA"
  project      = var.project
}

# creating service account secret key 
resource "google_service_account_key" "bigquery_sa_key" {
  service_account_id = google_service_account.bigquery_account.name
  public_key_type    = "TYPE_X509_PEM_FILE"
}

# move key to gcp secret manager 
resource "google_secret_manager_secret" "bq_sa_nro_secret" {
  secret_id = "${var.nro}-bigquery-sa-key"
  project   = var.project
  labels    = var.labels

  replication {
    user_managed {
      replicas {
        location = "europe-west1"
      }
    }
  }
}
# adding new version

resource "google_secret_manager_secret_version" "bq_nro_secret_version" {
  secret = google_secret_manager_secret.bq_sa_nro_secret.id

  secret_data = google_service_account_key.bigquery_sa_key.private_key
}


# assigning permissions
resource "google_project_iam_binding" "bigqueryDataEditor" {
  project = var.project
  role    = "roles/bigquery.dataEditor"

  members = [
    "serviceAccount:${google_service_account.bigquery_account.email}",
    "serviceAccount:${google_service_account.instance_account.email}"
  ]
}

resource "google_project_iam_binding" "storage" {
  project = var.project
  role    = "roles/storage.admin"
  members = [
    "serviceAccount:${google_service_account.bigquery_account.email}",
    "serviceAccount:${google_service_account.instance_account.email}"
  ]
}

resource "google_project_iam_binding" "bigqueryUser" {
  project = var.project
  role    = "roles/bigquery.user"
  members = [
    "serviceAccount:${google_service_account.bigquery_account.email}",
    "serviceAccount:${google_service_account.instance_account.email}"
  ]
}
