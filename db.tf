# resource "google_sql_database" "airbyte_database" {
#   name     = "${var.nro}-airbyte-db-instance"
#   project  = var.project
#   instance = google_sql_database_instance.instance.name
#   depends_on = [
#     google_sql_user.users
#   ]
# }

# # See versions at https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database_instance#database_version
# resource "google_sql_database_instance" "instance" {
#   name             = "${var.nro}-airbyte-db-instance"
#   region           = var.region
#   database_version = var.sql_version
#   project          = var.project
#   settings {
#     user_labels = var.labels
#     tier        = "db-f1-micro"
#     database_flags {
#       name  = "max_connections"
#       value = 200
#     }
#     ip_configuration {
#       ipv4_enabled    = false
#       private_network = google_compute_network.vpc.self_link
#       authorized_networks {
#         name  = "Cosmos-airbyte-federated"
#         value = google_compute_global_address.static.address
#       }
#     }
#   }

#   deletion_protection = "false"
#   depends_on = [
#     google_service_networking_connection.private_vpc_connection
#   ]
# }

# resource "random_password" "pass" {
#   length           = 16
#   special          = true
#   override_special = "!#$%&*()-_=+[]{}<>:?"
# }

# resource "google_sql_user" "users" {
#   name            = "${var.nro}-airbyte-db-user"
#   project         = var.project
#   deletion_policy = "ABANDON"
#   instance        = google_sql_database_instance.instance.name
#   password        = random_password.pass.result
# }
