locals {
  dns_zone_name = "${var.nro}.${var.dns_primary_zone}"
}

data "google_compute_image" "airbyte_image" {
  name    = var.image_name
  project = var.project
}

data "google_dns_managed_zone" "primary_dns_zone" {
  name    = var.dns_primary_zone_name
  project = var.dns_project
}

resource "google_compute_instance_template" "default" {
  name_prefix = "${var.nro}-airbyte-template-" # Use name prefix for create before destroy lifecycle policy
  project     = var.project
  disk {
    auto_delete  = true
    boot         = true
    device_name  = "persistent-disk-0"
    mode         = "READ_WRITE"
    disk_size_gb = var.disk_size_gb
    source_image = data.google_compute_image.airbyte_image.self_link
    type         = "PERSISTENT"
  }
  labels       = var.labels
  machine_type = var.machine_size
  metadata = {
    startup-script = "#! /bin/bash\n     echo '${data.template_file.env_file.rendered}' > /home/cosmosadmin/airbyte/.env\n cd /home/cosmosadmin/airbyte\n ./run-ab-platform.sh -d \n  docker compose up -d\n"
  }
  network_interface {
    access_config {
    }
    network            = google_compute_network.vpc.name
    subnetwork         = google_compute_subnetwork.private_subnet_1.name
    subnetwork_project = var.project
  }
  region = var.region
  scheduling {
    automatic_restart   = true
    on_host_maintenance = "MIGRATE"
    provisioning_model  = "STANDARD"
  }
  service_account {
    email  = google_service_account.instance_account.email
    scopes = ["https://www.googleapis.com/auth/devstorage.read_only", "https://www.googleapis.com/auth/logging.write", "https://www.googleapis.com/auth/monitoring.write", "https://www.googleapis.com/auth/pubsub", "https://www.googleapis.com/auth/service.management.readonly", "https://www.googleapis.com/auth/servicecontrol", "https://www.googleapis.com/auth/trace.append"]
  }
  tags = ["allow-health-check"]
  lifecycle {
    create_before_destroy = true
  }
}



resource "google_compute_instance_group_manager" "module_group" {
  name    = "${var.nro}-airbyte-module-instance-group-managed"
  zone    = var.zone
  project = var.project
  named_port {
    name = "http"
    port = "8000"
  }

  named_port {
    name = "https"
    port = "8443"
  }

  # named_port {
  #   name = "grafana"
  #   port = "3000"
  # }

  # named_port {
  #   name = "prometheus"
  #   port = "9090"
  # }

  version {
    instance_template = google_compute_instance_template.default.id
    name              = "primary"
  }
  base_instance_name = "${var.nro}-airbyte"
  target_size        = 1
  update_policy {
    type = "PROACTIVE"
    # instance_redistribution_type   = "PROACTIVE"
    minimal_action                 = "REPLACE"
    max_unavailable_fixed          = 3
    replacement_method             = "RECREATE"
    most_disruptive_allowed_action = "REPLACE"
  }
}


resource "google_compute_address" "static" {
  name       = "${var.nro}-airbyte-vm-public-address"
  project    = var.project
  region     = var.region
  depends_on = [google_compute_firewall.webserverrule]
}


resource "google_compute_global_address" "static" {
  project    = var.project
  name       = "${var.nro}-airbyte-lb-ipv4-1"
  ip_version = "IPV4"
}


resource "google_compute_firewall" "firewall" {
  name    = "${var.nro}-airbyte-gritfy-firewall-externalssh"
  network = google_compute_network.vpc.name
  project = var.project
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"] # Not So Secure. Limit the Source Range
  # target_tags   = []
}

resource "google_compute_firewall" "webserverrule" {
  name    = "${var.nro}-airbyte-gritfy-webserver"
  network = google_compute_network.vpc.name
  project = var.project
  allow {
    protocol = "tcp"
    ports    = ["443","8000","8001","8003","8006"]
  }
  source_ranges = [var.web_server_rules_source_range_1, var.web_server_rules_source_range_2]
}


resource "google_compute_health_check" "default" {
  name               = "${var.nro}-airbyte-http-basic-check"
  project            = var.project
  check_interval_sec = 5
  healthy_threshold  = 2
  tcp_health_check {
    port               = 8000
    port_specification = "USE_FIXED_PORT"
    proxy_header       = "NONE"
    # request_path       = "/"
  }
  timeout_sec         = 5
  unhealthy_threshold = 2
}


# resource "google_compute_health_check" "grafana" {
#   name               = "${var.nro}-grafana-basic-check"
#   project            = var.project
#   check_interval_sec = 5
#   healthy_threshold  = 2
#   tcp_health_check {
#     port               = 3000
#     port_specification = "USE_FIXED_PORT"
#     proxy_header       = "NONE"
#     # request_path       = "/"
#   }
#   timeout_sec         = 5
#   unhealthy_threshold = 2
# }

# resource "google_compute_health_check" "prometheus" {
#   name               = "${var.nro}-prometheus-basic-check"
#   project            = var.project
#   check_interval_sec = 5
#   healthy_threshold  = 2
#   tcp_health_check {
#     port               = 9090
#     port_specification = "USE_FIXED_PORT"
#     proxy_header       = "NONE"
#     # request_path       = "/"
#   }
#   timeout_sec         = 5
#   unhealthy_threshold = 2
# }


resource "google_compute_url_map" "default" {
  name            = "${var.nro}-airbyte-web-map-https"
  default_service = google_compute_backend_service.module_service.id
  project         = var.project

  host_rule {
    hosts        = ["airbyte.${local.dns_zone_name}"]
    path_matcher = "${var.nro}-airbyte"
  }

  # host_rule {
  #   hosts        = ["grafana.${local.dns_zone_name}"]
  #   path_matcher = "${var.nro}-grafana"
  # }

  # host_rule {
  #   hosts        = ["prometheus.${local.dns_zone_name}"]
  #   path_matcher = "${var.nro}-prometheus"
  # }

  path_matcher {
    name            = "${var.nro}-airbyte"
    default_service = google_compute_backend_service.module_service.id

    path_rule {
      paths   = ["/*"]
      service = google_compute_backend_service.module_service.id
    }

  }

  # path_matcher {
  #   name            = "${var.nro}-grafana"
  #   default_service = google_compute_backend_service.grafana_service.id

  #   path_rule {
  #     paths   = ["/*"]
  #     service = google_compute_backend_service.grafana_service.id
  #   }

  # }

  # path_matcher {
  #   name            = "${var.nro}-prometheus"
  #   default_service = google_compute_backend_service.prometheus_service.id

  #   path_rule {
  #     paths   = ["/*"]
  #     service = google_compute_backend_service.prometheus_service.id
  #   }

  # }
}

resource "google_compute_managed_ssl_certificate" "airbyte_cert" {
  provider = google-beta
  name     = "${var.nro}-airbyte-ssl-cert"
  project  = var.project

  managed {
    domains = ["airbyte.${local.dns_zone_name}"]
  }
}

# resource "google_compute_managed_ssl_certificate" "grafana_cert" {
#   provider = google-beta
#   name     = "${var.nro}-grafana-ssl-cert"
#   project  = var.project

#   managed {
#     domains = ["grafana.${local.dns_zone_name}"]
#   }
# }

# resource "google_compute_managed_ssl_certificate" "prometheus_cert" {
#   provider = google-beta
#   name     = "${var.nro}-prometheus-ssl-cert"
#   project  = var.project

#   managed {
#     domains = ["prometheus.${local.dns_zone_name}"]
#   }
# }

resource "google_compute_target_https_proxy" "https_default" {
  name    = "${var.nro}-airbyte-https-lb-proxy"
  url_map = google_compute_url_map.default.id
  ssl_certificates = [
    google_compute_managed_ssl_certificate.airbyte_cert.name,
    # google_compute_managed_ssl_certificate.grafana_cert.name
    # google_compute_managed_ssl_certificate.prometheus_cert.name
  ]
  depends_on = [
    google_compute_managed_ssl_certificate.airbyte_cert,
    # google_compute_managed_ssl_certificate.grafana_cert
    # google_compute_managed_ssl_certificate.prometheus_cert
  ]
  project = var.project
}

resource "google_compute_target_http_proxy" "http_default" {
  name    = "${var.nro}-airbyte-http-lb-proxy"
  url_map = google_compute_url_map.default.id
  project = var.project
}

resource "google_compute_global_forwarding_rule" "https_default" {
  name                  = "${var.nro}-airbyte-https-content-rule"
  project               = var.project
  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL"
  port_range            = "443-443"
  target                = google_compute_target_https_proxy.https_default.id
  ip_address            = google_compute_global_address.static.address
}

# resource "google_compute_global_forwarding_rule" "airbyte_internal" {
#   name                  = "${var.nro}-airbyte-internal-rule"
#   project               = var.project
#   ip_protocol           = "TCP"
#   load_balancing_scheme = "EXTERNAL"
#   port_range            = "8000-8006"
#   target                = google_compute_target_http_proxy.http_default.id
#   ip_address            = google_compute_global_address.static.address
# }


resource "google_compute_address" "nat-ip" {
  name    = "${var.nro}-airbyte-nap-ip"
  project = var.project
  region  = var.region
}

# create a nat to allow private instances connect to internet
resource "google_compute_router" "nat-router" {
  name    = "${var.nro}-airbyte-nat-router"
  network = google_compute_network.vpc.name
  project = var.project
  region  = var.region
}

resource "google_compute_router_nat" "nat-gateway" {
  name                               = "${var.nro}-airbyte-nat-gateway"
  router                             = google_compute_router.nat-router.name
  nat_ip_allocate_option             = "MANUAL_ONLY"
  nat_ips                            = [google_compute_address.nat-ip.self_link]
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
  depends_on                         = [google_compute_address.nat-ip]
  project                            = var.project
  region                             = var.region
}

resource "google_compute_network" "vpc" {
  name                    = "${var.nro}-airbyte-vpc"
  auto_create_subnetworks = "false"
  routing_mode            = "GLOBAL"
  project                 = var.project
}

resource "google_compute_global_address" "private_ip_block" {
  name          = "${var.nro}-airbyte-private-ip-block"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  ip_version    = "IPV4"
  prefix_length = 20
  network       = google_compute_network.vpc.self_link
  project       = var.project
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = google_compute_network.vpc.self_link
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_block.name]
}

resource "google_compute_subnetwork" "private_subnet_1" {
  purpose       = "PRIVATE"
  name          = "${var.nro}-airbyte-private-subnet-1"
  ip_cidr_range = "10.2.0.0/16"
  network       = google_compute_network.vpc.name
  region        = var.region
  project       = var.project
}

resource "google_compute_backend_service" "module_service" {
  name      = "${var.nro}-airbyte-module-service"
  port_name = "http"
  protocol  = "HTTP"
  project   = var.project

  backend {
    group = google_compute_instance_group_manager.module_group.instance_group
  }

  health_checks = [
    google_compute_health_check.default.id,
  ]
}

# resource "google_compute_backend_service" "prometheus_service" {
#   name      = "${var.nro}-prometheus-service"
#   port_name = "prometheus"
#   protocol  = "HTTP"
#   project   = var.project

#   backend {
#     group = google_compute_instance_group_manager.module_group.instance_group
#   }

#   health_checks = [
#     google_compute_health_check.prometheus.id,
#   ]
# }

# resource "google_compute_backend_service" "grafana_service" {
#   name      = "${var.nro}-grafana-service"
#   port_name = "grafana"
#   protocol  = "HTTP"
#   project   = var.project

#   backend {
#     group = google_compute_instance_group_manager.module_group.instance_group
#   }

#   health_checks = [
#     google_compute_health_check.grafana.id,
#   ]
# }
resource "random_password" "password" {
  length  = 16
  special = false
  # override_special = "!#$%&*()-_=+[]{}<>:?"
}

data "template_file" "env_file" {
  template = file(var.env_file_path)
  vars = {
    AIRBYTE_VERSION     = var.airbyte_version
    BASIC_AUTH_USERNAME = "${var.nro}-admin",
    BASIC_AUTH_PASSWORD = random_password.password.result,
    DATABASE_USER       = "docker",
    DATABASE_PASSWORD   = "docker",
    DATABASE_DB         = "airbyte"
    DATABASE_HOST       = "db"
  }
}




data "google_iam_policy" "admin" {
  binding {
    role = "roles/iap.httpsResourceAccessor"
    # members = jsondecode(var.iap_iam_https_resource_accessor_members) # 
    members = var.iap_iam_https_resource_accessor_members
  }
}

# Resouce to allow IAM user to access airbyte
resource "google_iap_web_backend_service_iam_policy" "airbyte_policy" {
  project             = var.project
  web_backend_service = basename(google_compute_backend_service.module_service.name) # using basename function due to issue in the provider - https://github.com/hashicorp/terraform-provider-google/issues/4515
  policy_data         = data.google_iam_policy.admin.policy_data
}

# # accessing prometheus
# resource "google_iap_web_backend_service_iam_policy" "prometheus_policy" {
#   project             = var.project
#   web_backend_service = basename(google_compute_backend_service.prometheus_service.name)
#   policy_data         = data.google_iam_policy.admin.policy_data
# }
# Accessing grafana
# resource "google_iap_web_backend_service_iam_policy" "grafana_policy" {
#   project             = var.project
#   web_backend_service = basename(google_compute_backend_service.grafana_service.name)
#   policy_data         = data.google_iam_policy.admin.policy_data
# }

#DNS
# resource "google_dns_managed_zone" "managed_zone" {
#   project  = var.dns_project
#   name     = "${var.nro}-airbyte-zone"
#   dns_name = "${local.dns_zone_name}."
# }

resource "google_dns_record_set" "airbyte_dns_record" {
  project = var.dns_project
  name    = "airbyte.${local.dns_zone_name}."
  type    = "A"
  ttl     = 300

  managed_zone = "${var.nro}-zone"

  depends_on = [
    google_compute_global_forwarding_rule.https_default
  ]

  rrdatas = ["${google_compute_global_forwarding_rule.https_default.ip_address}"]
}

# resource "google_dns_record_set" "prometheus_dns_record" {
#   project = var.project
#   name    = "prometheus.${google_dns_managed_zone.managed_zone.dns_name}"
#   type    = "A"
#   ttl     = 300

#   managed_zone = google_dns_managed_zone.managed_zone.name

#   depends_on = [
#     google_compute_global_forwarding_rule.https_default,
#     google_compute_global_forwarding_rule.http_default
#   ]

#   rrdatas = ["${google_compute_global_forwarding_rule.https_default.ip_address}"]
# }

# resource "google_dns_record_set" "grafana_dns_record" {
#   project = var.dns_project
#   name    = "grafana.${google_dns_managed_zone.managed_zone.dns_name}"
#   type    = "A"
#   ttl     = 300

#   managed_zone = google_dns_managed_zone.managed_zone.name

#   depends_on = [
#     google_compute_global_forwarding_rule.https_default,
#     google_compute_global_forwarding_rule.http_default
#   ]

#   rrdatas = ["${google_compute_global_forwarding_rule.https_default.ip_address}"]
# }

# resource "google_dns_record_set" "zone_ns_record" {
#   project = var.dns_project
#   name    = "${local.dns_zone_name}."
#   type    = "NS"
#   ttl     = 300

#   managed_zone = data.google_dns_managed_zone.primary_dns_zone.name

#   depends_on = [
#     google_dns_managed_zone.managed_zone
#   ]

#   rrdatas = google_dns_managed_zone.managed_zone.name_servers
# }
